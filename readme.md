### Snake
Simple snake game that runs in terminal using ncurses.

![Snake Running](./docs/snake.gif?raw=true "Running")

### Get
```
$ git clone --recursive https://gitlab.com/_mobius/snake_nc.git
```

### Installing depenendencies
```
$ apt install libncurses5-dev libncursesw5-dev
```

### Compiling
```bash
$ mkdir build  
$ cd build  
$ cmake -DCMAKE_BUILD_TYPE=Release ..  
$ cmake --build .  
```

### Controls
* _pause_: __p__
* _move_: __arrows__
* _quit_: __end__
* _reset_: __r__
