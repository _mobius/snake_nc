/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <algorithm>
#include <random>
#include <limits>
#include <vector>
#include <thread>

#include "tglib.h"

using namespace tgl;

// true ifsome `pt` is in the list
bool collision( const Vec2& pt, const std::vector<Vec2>& pl )
{
    return std::find( begin( pl ), end( pl ), pt ) != end( pl );
}

class Snake
{
    public:
        bool collide( const Vec2& p ) const
        {
            return collision( p, segments );
        }

        const Vec2& head() const { return segments.front(); }
        Vec2& head() { return segments.front(); }

        void head( const Vec2& new_pos ) { segments.front() = new_pos; }

        const Vec2& tail() const { return segments.back(); }
        const Vec2& tail() { return segments.back(); }

        auto length() const { return segments.size(); }

        void reset( const Vec2& head )
        {
            segments.clear();
            segments.push_back( head );
        }

    public:
        std::vector<Vec2>   segments{};                       // snake segments
        Vec2                direction{ 1, 0 };                // movement direction
        Color_Pair          color{ Color::red, Color::red };  // segment color
};

class Rand
{
    public:
        Rand( int32_t low, int32_t high ): _dis{ low, high } {}

        auto get() { return _dis( _gen ); }

    private:
        std::random_device                     _rd{};
        std::mt19937                           _gen{ _rd() };
        std::uniform_int_distribution<int32_t> _dis;
};

enum class Collision_Type { none = 0, food, snake, wall, boundry };

// true on no collision or non-terminal collision (food)
bool safe_collision( Collision_Type ct )
{
    return static_cast<char>( ct ) <= static_cast<char>( Collision_Type::food );
}

enum class Game_State { running, paused, game_over };

class Snake_Game
{
    public:
        Snake_Game( uint8_t snake_size = 2, uint8_t max_food = 2 ): _init_size{ snake_size }
        {
            _init_snake();
            _food.reserve( max_food );

            _create_walls();
        }

        void reset()
        {
            surface.set_pixels( _food, Color::none );
            surface.set_pixels( snake.segments, Color::none );

            _food.clear();
            _init_snake();

            spawn_food();

            _dead = false;
        }

        bool grow_snake()
        {
            // get the direction the tail is pointing
            auto tail_dir = [this]()
            {
                // single segment snake, tail 
                if( snake.segments.size() <= 1 )
                {
                    return -snake.direction;
                }

                auto tail = std::rbegin( snake.segments );
                auto t_1 = tail + 1;

                return *tail - *t_1;
            }();

            // potential growth points
            auto offsets = [this, &tail_dir]() -> std::array<Vec2, 5>
            {
                auto &tail = snake.tail();

                return {{
                    tail + tail_dir,    // desired placement
                    tail + Vec2{ 1, 0 }, tail + Vec2{ -1, 0 }, // surrounding points
                    tail + Vec2{ 0 , 1 }, tail + Vec2{ 0, -1 }
                }};
            }();

            // find an open space for growth around tail
            auto new_tail = [&offsets, this]() -> Vec2
            {
                auto open = std::find_if( 
                                    begin( offsets ),
                                    end( offsets ),
                                    [this]( auto &o )
                                    {
                                        return collision_check( o ) == Collision_Type::none;
                                    } );

                return open == end( offsets ) ? snake.tail() : *open;
            }();

            // no room for growth
            if( new_tail == snake.tail() ) 
            {
                return false;
            }

            snake.segments.emplace_back( new_tail.x, new_tail.y );

            return true;
        }

        // check for collisions against everything
        Collision_Type collision_check( const Vec2& pt ) const
        {
            // surface boundries
            if( pt.x <= 0 || pt.x >= surface.width() || pt.y < 0 || pt.y >= surface.height() )
            {
                return Collision_Type::boundry;
            }

            if( snake.collide( pt ) )
            {
                return Collision_Type::snake;
            }

            if( collision( pt, _walls ) )
            {
                return Collision_Type::wall;
            }

            if( collision( pt, _food ) )
            {
                return Collision_Type::food;
            }

            return Collision_Type::none;
        }

        // attempt to move the snake
        bool move()
        {
            Vec2 next_pos = Vec2{ snake.head().x + snake.direction.x, snake.head().y + snake.direction.y };

            if( !safe_collision( collision_check( next_pos ) ) )
            {
                _dead = true;
                return false;
            }

            // clear last drawn
            surface.set_pixels( snake.segments, Color::none );

            // update positions
            std::copy_backward( begin( snake.segments ), end( snake.segments ) - 1, end( snake.segments ) );
            snake.head() = next_pos;

            return true;
        }

        void spawn_food() 
        {
            if( _food.size() < _food.capacity() )
            {
                Vec2 fp{ _rx.get(), _ry.get() };
                while( collision_check( fp ) != Collision_Type::none )
                {
                    fp.x = _rx.get();
                    fp.y = _ry.get();
                }

                _food.emplace_back( fp.x, fp.y );
            }
        };

        auto check_eat()
        {
            auto f = std::find( begin( _food ), end( _food ), snake.head() );

            if( f != end( _food ) )
            {
                _food.erase( f );
                grow_snake();
            }
        };

        void update()
        {
            move();
            check_eat();
            spawn_food();
        }

        void draw()
        {
            draw_snake();
            draw_food();
        }

        void draw_walls()
        {
            surface.set_pixels( _walls, Color::yellow );
        }

        void draw_snake()
        {
            surface.set_pixels( snake.segments, snake.color );
        }

        void draw_food()
        {
            surface.set_pixels( _food, Color::green );
        }

        uint32_t score() const 
        {
            return snake.length() - _init_size;
        }

        void present()
        {
            draw();
            surface.present();
        }

        // will not change direction ifit would self-collide (cannot turn back on self)
        bool change_direction( const Vec2& new_dir )
        {
            if( new_dir.x && new_dir.y ) { return false; } // can only move on 1 axis at a time

            auto &head = snake.head();
            Vec2 next{ head.x + new_dir.x, head.y + new_dir.y };

            // only change a direction ifnot already moving on that axis
            if( !snake.collide( next ) && !collision( next, _walls ) )
            {
                snake.direction = new_dir;
                return true;
            }

            return false;
        }

        bool dead() const { return _dead; }

    private:
        void _init_snake( )
        {
            snake.reset( { static_cast<int32_t>( surface.width() / 2 ), static_cast<int32_t>( surface.height() / 2 ) } );
            for( auto i = _init_size - 1; i; --i ) { grow_snake(); }
        }

        void _create_walls()
        {
            for( int32_t x = 0; x < surface.width(); ++x )
            {
                _walls.emplace_back( x, 0 );
                _walls.emplace_back( x, surface.height() - 1 );
            }

            for( int32_t y = 1; y < surface.height(); ++y )
            {
                _walls.emplace_back( 0, y );
                _walls.emplace_back( surface.width() - 1, y );
            }
        }

    public:
        Game_State  state{ Game_State::paused };
        Snake       snake{};

        Surface     surface{ 20, 40, 0, 0 };

    private:

        std::vector<Vec2>   _food{};
        std::vector<Vec2>   _walls{};

        uint32_t            _init_size{ 2 };

            // for randomly spawning food
        Rand _rx{ 1, static_cast<int32_t>( surface.width() - 1 ) },
             _ry{ 1, static_cast<int32_t>( surface.height() - 1 ) };

        bool    _dead{ false };
};


int main()
{
    using namespace std;

    NCurses nc{};
    Keyboard keyboard{};
    Snake_Game game{ 2, 2 };
    Game_State game_state{ Game_State::running };

    Boxed_Region score_box{ 15, 3, game.surface.width() * 2 + 1, 0 };

    Msg_Box game_over_box{ "Game Over", { game.surface.width(), game.surface.height() / 2 } };
    Msg_Box paused_box{ "Paused", { game.surface.width(), game.surface.height() / 2 } };

    game.draw_walls();
    while( true )
    {
        keyboard.update();

        if( keyboard.pressed( KEY_END ) || keyboard.pressed( 'q' ) )
        {
            break;
        }
        else if( keyboard.pressed( 'r' ) )
        {
            game.reset();

            game_state = Game_State::running;
            game_over_box.clear();
        }

        if( game_state == Game_State::running )
        {
            if( keyboard.pressed( KEY_BREAK ) || keyboard.pressed( 'p' ) )
            {
                game_state = game_state == Game_State::running ? Game_State::paused : Game_State::running;
                if( game_state == Game_State::paused )
                {
                    paused_box.present();
                }
                else 
                {
                    paused_box.clear();
                }
            }

            if( keyboard.pressed( KEY_LEFT ) ) { game.change_direction( { -1, 0 } ); }
            if( keyboard.pressed( KEY_RIGHT ) ) { game.change_direction( { 1, 0 } ); }
            if( keyboard.pressed( KEY_UP ) ) { game.change_direction( { 0, -1 } ); }
            if( keyboard.pressed( KEY_DOWN ) ) { game.change_direction( { 0, 1 } ); }

            game.update();
            game.present();

            if( game.dead() )
            {
                game_state = Game_State::game_over;
                game_over_box.present();
            }

            score_box.print( { 0, 0 }, "Score: %d", game.score() );
            score_box.present();
        }

        std::this_thread::sleep_for( std::chrono::milliseconds( 80 ) );
    }

    auto key = getch();

    return 0;
}
